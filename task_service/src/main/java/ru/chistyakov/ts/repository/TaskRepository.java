package ru.chistyakov.ts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chistyakov.ts.dto.TaskDto;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<TaskDto,String> {

     List<TaskDto> getTaskDtoByProjectId(String projectId);
}
