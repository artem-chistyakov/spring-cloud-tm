package ru.chistyakov.ts.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name = "tasks")
public class TaskDto {

    @Id
    private String id;

    private String title;

    @Column(name = "project_id")
    private String projectId;
    public TaskDto() {
        this.id = UUID.randomUUID().toString();
    }
}
