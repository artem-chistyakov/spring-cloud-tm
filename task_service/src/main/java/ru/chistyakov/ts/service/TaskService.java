package ru.chistyakov.ts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chistyakov.ts.dto.TaskDto;
import ru.chistyakov.ts.repository.TaskRepository;

import java.util.List;

@Service

public class TaskService {
    private TaskRepository taskRepository;

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<TaskDto> getTaskByProjectId(String projectId) {
        if (projectId == null) return null;
        return taskRepository.getTaskDtoByProjectId(projectId);
    }
    public List<TaskDto> getAllTask(){
        return taskRepository.findAll();
    }
}
