package ru.chistyakov.ts.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.chistyakov.ts.dto.TaskDto;
import ru.chistyakov.ts.service.TaskService;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskEndpoint {

    private TaskService taskService;

    @Autowired
    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    List<TaskDto> getAllTask(){
        return taskService.getAllTask();
    }
    @GetMapping("/{projectId}")
    List<TaskDto> getTaskByProjectId(@PathVariable String projectId) {
        return taskService.getTaskByProjectId(projectId);
    }
}
