package ru.chistyakov.sus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SEurekaServer {
    public static void main(String[] args) {
        SpringApplication.run(SEurekaServer.class);
    }
}
