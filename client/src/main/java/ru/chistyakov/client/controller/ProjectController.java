package ru.chistyakov.client.controller;

import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.chistyakov.client.dto.ProjectDto;

@Controller
@RequestMapping("/projects")
@PropertySource("classpath:application.properties")
public class ProjectController {

    @Value("${gateway.url.project}")
    private String projectUrl;

    @Autowired
    KeycloakRestTemplate keycloakRestTemplate;

    @GetMapping
    public String getAllProject(Model model) {
        ResponseEntity<ProjectDto[]> responseEntity = keycloakRestTemplate.getForEntity(projectUrl + "projects/", ProjectDto[].class);
        model.addAttribute("projectList", responseEntity.getBody());
        return "projectListView";
    }

    @GetMapping("/{id}")
    public String getOneProject(@PathVariable("id") String projectId, Model model) {
        ProjectDto responseEntity = keycloakRestTemplate.getForObject(projectUrl + "projects/" + projectId, ProjectDto.class);
        System.out.println(responseEntity);
        model.addAttribute("projectDto", responseEntity);
        return "projectView";
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public void createProject(@ModelAttribute("title") String title){
        ProjectDto projectDto = new ProjectDto();
        projectDto.setTitle(title);
    }
}
