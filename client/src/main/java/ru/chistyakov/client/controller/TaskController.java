package ru.chistyakov.client.controller;

import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.chistyakov.client.dto.ProjectDto;

@Controller
@RequestMapping("/tasks")
@PropertySource("classpath:application.properties")
public class TaskController {

    @Value("${gateway.url.task}")
    private String taskUrl;

    @Autowired
    KeycloakRestTemplate keycloakRestTemplate;

    @GetMapping
    public String getAllTask(Model model) {
        ResponseEntity<ProjectDto[]> responseEntity = keycloakRestTemplate.getForEntity(taskUrl + "projects/", ProjectDto[].class);
        model.addAttribute("taskList", responseEntity.getBody());
        return "taskListView";
    }
}