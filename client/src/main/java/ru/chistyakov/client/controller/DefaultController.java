package ru.chistyakov.client.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DefaultController {

    @GetMapping("/logout")
    public String logout(HttpServletRequest httpServletRequest) throws Exception {
        httpServletRequest.logout();
        return "index";
    }
}
