package ru.chistyakov.client.entity;

import lombok.Data;

@Data
public class TaskDto {
    private String id;
    private String title;
    private String projectId;
}
