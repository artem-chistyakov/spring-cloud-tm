package ru.chistyakov.client.entity;

import lombok.Data;

@Data
public class ProjectDto {
    private String id;
    private String title;
}
