package ru.chistyakov.client.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ProjectDto {
    private String id;
    private String title;

    public ProjectDto() {
        this.id = UUID.randomUUID().toString();
    }
}
