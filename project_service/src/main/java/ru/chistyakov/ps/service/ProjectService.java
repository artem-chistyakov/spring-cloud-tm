package ru.chistyakov.ps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chistyakov.ps.dto.ProjectDto;
import ru.chistyakov.ps.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService {

    private ProjectRepository projectRepository;

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ProjectDto getProject(String projectId){
        return projectRepository.getOne(projectId);
    }
    public void createProject(ProjectDto projectDto){
        projectRepository.save(projectDto);
    }
    public List<ProjectDto> getAllProject() {
        return projectRepository.findAll();
    }
}
