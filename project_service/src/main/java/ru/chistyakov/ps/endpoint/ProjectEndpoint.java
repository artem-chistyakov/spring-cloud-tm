package ru.chistyakov.ps.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.chistyakov.ps.dto.ProjectDto;
import ru.chistyakov.ps.dto.TaskDto;
import ru.chistyakov.ps.feignClient.TaskFeignClient;
import ru.chistyakov.ps.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping("/projects")
public class ProjectEndpoint {

    private ProjectService projectService;
    private TaskFeignClient taskFeignClient;

    @Autowired
    public void setTaskFeignClient(TaskFeignClient taskFeignClient) {
        this.taskFeignClient = taskFeignClient;
    }

    @Autowired
    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public List<ProjectDto> getAllProject() {
        return projectService.getAllProject();
    }

    @GetMapping("/{id}")
    public ProjectDto getProject(@PathVariable("id")String projectId) {
        return projectService.getProject(projectId);
    }

    @RequestMapping("/tasks/{projectId}")
    public List<TaskDto> getTaskByProjectId(@PathVariable("projectId") String projectId) {
        return taskFeignClient.getTaskByProjectId(projectId);
    }
    @PostMapping
    public void createProject(ProjectDto projectDto){
        projectService.createProject(projectDto);
    }
}
