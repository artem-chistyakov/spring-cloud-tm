package ru.chistyakov.ps.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.chistyakov.ps.dto.ProjectDto;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectDto, String> {

}
