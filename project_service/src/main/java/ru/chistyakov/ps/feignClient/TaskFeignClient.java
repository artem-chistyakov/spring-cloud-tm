package ru.chistyakov.ps.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.chistyakov.ps.dto.TaskDto;

import java.util.List;

@FeignClient(name = "sct-task-service")
public interface TaskFeignClient {

    @GetMapping("/tasks/{projectId}")
    List<TaskDto> getTaskByProjectId(@PathVariable("projectId") String projectId);

}
