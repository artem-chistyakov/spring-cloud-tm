package ru.chistyakov.ps.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name = "projects")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProjectDto {

    @Id
    private final String id;
    private String title;

    public ProjectDto() {
        this.id = UUID.randomUUID().toString();
    }
}
