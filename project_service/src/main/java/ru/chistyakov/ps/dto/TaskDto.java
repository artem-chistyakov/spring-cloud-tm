package ru.chistyakov.ps.dto;

import lombok.Data;

@Data
public class TaskDto {

    private String id;

    private String title;

    private String projectId;
}
